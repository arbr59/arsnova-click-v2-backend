export enum QuestionType {
  ABCDSingleChoiceQuestion      = 'ABCDSingleChoiceQuestion', //
  FreeTextQuestion              = 'FreeTextQuestion', //
  MultipleChoiceQuestion        = 'MultipleChoiceQuestion', //
  RangedQuestion                = 'RangedQuestion', //
  SingleChoiceQuestion          = 'SingleChoiceQuestion', //
  SurveyQuestion                = 'SurveyQuestion', //
  TrueFalseSingleChoiceQuestion = 'TrueFalseSingleChoiceQuestion', //
  YesNoSingleChoiceQuestion     = 'YesNoSingleChoiceQuestion', //
}
