export enum RoutingCache {
  QuizAssets,
  QuizFullStatus,
  QuizStatus,
  ApiDoc,
  QuizPoolTags,
  BlockedNicks,
  PredefinedNicks,
  Statistics,
  ActiveQuizzes,
  QuizData,
  MemberGroup,
  QuizExportSheet,
  CurrentQuizState,
  QuizStartTime,
  AssetFiles,
  QuizSettings
}
